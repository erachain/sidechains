# Blockchain by Erachain technology - Sidechain (Clonechain)
How make Your own blockchain with KYC, exchange, assets, polls etc.
For start Your own blockchain put `sideGENESIS.json` file into application folder. See examples in `z_SIDECHAIN_EXAMPLES` folder.

## Ключи запуска ноды
https://gitlab.com/erachain/Erachain-Node

## Кратко про Sidechain (Clonechain)
Это свой собственный блокчейн с самого первого блока со всем функционалом платформы Erachain

Более подробно тут https://vk.com/@erachain-erachain-50-obzor-novyh-vozmozhnostei

## Термины и определения платформы Erachain
Для быстрого понимания как настроить sidechain и что такое блокчейн вообще, сначала загляните сюда:  

https://gitlab.com/erachain/sidechains/-/blob/master/terms.md


## Описание по сайдчейнам
Описание как запускать и как настраивать здесь:  

https://gitlab.com/erachain/sidechains/-/blob/master/start.md

## Уроки по сайдчейнам
+ как быстро создать свой блокчейн: https://www.youtube.com/watch?v=4OIGrRC-uJU&list=PL9gtTYrhfx8Ug80EHNgDO7bgh3pzs8k_5&index=7&t=0s  
+ как запустить блокчейн-ноду на виртуальной машине (в облаке): https://www.youtube.com/watch?v=N-UXedtPCEE&list=PL9gtTYrhfx8Ug80EHNgDO7bgh3pzs8k_5

## Статьи по сайдчейнам

https://cryptorussia.ru/news/kazhdomu-po-blokcheynu-erachain-gotovit-reliz-dlya-zapuska-otdelnyh-cepochek

## API & RPC для создания платежей
https://gitlab.com/d.ermolaev/erachain-rpc



